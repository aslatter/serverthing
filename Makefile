
CC ?= gcc

CFLAGS := -ggdb -fno-omit-frame-pointer
CFLAGS += -Wall -Wextra -Werror -Wformat=2
CFLAGS += -pthread
CFLAGS += -MMD -MP
CLFAGS += -fsanitize=address -fsanitize=undefined

LDFLAGS := -lpthread -pthread
LDFLAGS += -fsanitize=address -fsanitize=undefined

exes := bin/client bin/server bin/test
ofiles := obj/io.o obj/db.o obj/str.o

syms := $(addsuffix .txt,$(addprefix ref/,$(basename $(notdir $(ofiles)))))

all: $(exes) $(syms)

obj/%.o: src/%.c Makefile
	@ echo " CC $<"
	@ mkdir -p obj
	@ $(CC) $(CFLAGS) -c $< -o $@

-include $(wildcard obj/*.d)

$(exes) : bin/% : obj/%.o $(ofiles)
	@ echo " LD $@"
	@ mkdir -p bin
	@ $(CC) $(LDFLAGS) -o $@ $< $(ofiles)

# track names of public symbols
ref/%.txt: obj/%.o
	@ echo " REF $@"
	@ mkdir -p ref
	@ nm -g $< | grep -v \\bU\\b | awk '{print $$3}' | sort > $@

test: bin/test
	@ bin/test

clean:
	@ $(RM) -r obj
	@ $(RM) -r bin

format:
	@ clang-format -i src/*.c src/*.h

tidy:
	@ clang-tidy --fix -checks=-\*,bugprone-\* src/*.c src/*.h -- $(CFLAGS)

.PHONY: all clean format tidy test

