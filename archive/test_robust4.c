/*

Example program illustrating the use of a "robust" mutex across processes.

The mutex is stored in a memfd memory-region, the memfd is then shared
between the two processes across a file-socket.

*/

// required for memfd_create from sys/mman (the man pages lie -
// I don't seem to be able to import memfd_create from sys/memfd)
#define _GNU_SOURCE

#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#include "io.h"

static void setup_socket_server();
static void init_mutex();
static void abandon_mutext();
static void remap_mutext();
static void child_get_shm();
static void setup_temp_file();

static char temp_dir_name[1000];
static char temp_file_name[1000];
static int sock;
static int shm_fd;
static struct {
  char x[500];
  pthread_mutex_t m;
  char y[500];
} * s;

static int mmap_prot = PROT_READ | PROT_WRITE;
static int mmap_flags = MAP_SHARED;

int main() {
  setup_temp_file();
  init_mutex();
  abandon_mutext();
  remap_mutext();

  // attempt to acquire mutex, check for OWNERDEAD
  int result = pthread_mutex_lock(&s->m);
  printf("result := %s\n", strerror(result));

  unlink(temp_file_name);
  rmdir(temp_dir_name);

  if (result == EOWNERDEAD) {
    exit(EXIT_SUCCESS);
  } else {
    exit(EXIT_FAILURE);
  }
}

void setup_temp_file() {
  strncpy(temp_dir_name, "/tmp/testsock.XXXXXX", sizeof(temp_dir_name) - 1);
  if (mkdtemp(temp_dir_name) == NULL) {
    perror("mkdtemp");
    exit(EXIT_FAILURE);
  }
  strncpy(temp_file_name, temp_dir_name, sizeof(temp_file_name) - 1);
  strncat(temp_file_name, "/sock",
          sizeof(temp_file_name) - strlen(temp_file_name) - 1);
}

void init_mutex() {

  // memfd shared with child process
  shm_fd = memfd_create("test memfd", 0);
  ftruncate(shm_fd, sizeof(*s));

  // init memory for mutex
  s = mmap(NULL, sizeof(*s), mmap_prot, mmap_flags, shm_fd, 0);

  // init mutext
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);

  pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);
  pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);

  pthread_mutex_init(&s->m, &attr);

  pthread_mutexattr_destroy(&attr);

  // un-map - we let the child process map it itself
  munmap(s, sizeof(*s));
  s = NULL;
}

void remap_mutext() {
  s = mmap(NULL, sizeof(*s), mmap_prot, mmap_flags, shm_fd, 0);
}

void abandon_mutext() {
  setup_socket_server();

  pid_t fork_result = fork();
  if (fork_result == -1) {
    perror("fork");
    exit(EXIT_FAILURE);
  }

  if (fork_result == 0) {
    // child - we want our own fresh copy of
    // the shared-memory fd, so we will get it over
    // a file-socket from the parent

    close(sock);
    sock = -1;

    close(shm_fd);
    shm_fd = -1;

    child_get_shm();
    remap_mutext();
    pthread_mutex_lock(&s->m);

    exit(EXIT_SUCCESS);
  } else {
    // parent

    // send shared-memory fd to child over file-socket
    int child_sock = accept(sock, NULL, NULL);
    if (child_sock != -1) {
      send_fd(child_sock, shm_fd);
      close(child_sock);
    }

    // wait for child process to be done
    do {
      int wait_result = waitpid(fork_result, NULL, 0);
      if (wait_result == fork_result) {
        return;
      }
      if (wait_result == -1) {
        if (errno == EINTR) {
          continue;
        } else {
          perror("wait");
          exit(EXIT_FAILURE);
        }
      }
      // ??
      fprintf(stderr, "Unexpected return from waitpid\n");
      exit(EXIT_FAILURE);
    } while (true);
  }
}

void setup_socket_server() {

  // bind socket to file
  if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("open");
    exit(EXIT_FAILURE);
  }
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;

  strncpy(addr.sun_path, temp_file_name, sizeof(addr.sun_path));

  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("bind");
    exit(EXIT_FAILURE);
  }

  const int backlog = 1;
  if (listen(sock, backlog) != 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }
}

void child_get_shm() {

  // Connect to parent process over a socket

  if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("open");
    exit(EXIT_FAILURE);
  }
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;

  strncpy(addr.sun_path, temp_file_name, sizeof(addr.sun_path));

  if (connect(sock, &addr, sizeof(addr)) != 0) {
    perror("connect");
    exit(EXIT_FAILURE);
  }

  // The whole point - get the shared-memory fd from parent
  receive_fd(sock, &shm_fd);

  close(sock);
}
