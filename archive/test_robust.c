/*

Example program illustrating the use of a "robust" mutex used by two threads.

*/

#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void init_mutex();
static void abandon_mutext();
static void *do_abondon_mutex(void *arg);

static pthread_mutex_t m;

int main() {
  init_mutex();
  abandon_mutext();

  // attempt to aquire mutex, check for OWNERDEAD
  int result = pthread_mutex_lock(&m);
  printf("result := %s\n", strerror(result));
  if (result == EOWNERDEAD) {
    exit(EXIT_SUCCESS);
  } else {
    exit(EXIT_FAILURE);
  }
}

void init_mutex() {
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
  pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);

  pthread_mutex_init(&m, &attr);

  pthread_mutexattr_destroy(&attr);
}

void abandon_mutext() {
  pthread_t child;
  int err;
  if ((err = pthread_create(&child, NULL, do_abondon_mutex, NULL)) != 0) {
    fprintf(stdout, "pthread_create: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }

  if ((err = pthread_join(child, NULL)) != 0) {
    fprintf(stdout, "pthread_join: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }
}

void *do_abondon_mutex(void *arg) {
  (void)arg;
  pthread_mutex_lock(&m);
  pthread_exit(NULL);
  return NULL;
}
