/*

Example program illustrating the use of a "robust" mutex across processes.

The mutex is stored in a memfd memory-region, the memfd is then shared
between the two processes.

*/

// required for memfd_create from sys/mman (the man pages lie -
// I don't seem to be able to import memfd_create from sys/memfd)
#define _GNU_SOURCE

#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static void init_mutex();
static void abandon_mutext();
static void remap_mutext();

static int fd;
static pthread_mutex_t *m;

static int mmap_prot = PROT_READ | PROT_WRITE;
static int mmap_flags = MAP_SHARED;

int main() {
  init_mutex();
  abandon_mutext();
  remap_mutext();

  // attempt to acquire mutex, check for OWNERDEAD
  int result = pthread_mutex_lock(m);
  printf("result := %s\n", strerror(result));
  if (result == EOWNERDEAD) {
    exit(EXIT_SUCCESS);
  } else {
    exit(EXIT_FAILURE);
  }
}

void init_mutex() {

  // memfd shared with child process
  fd = memfd_create("test memfd", 0);
  ftruncate(fd, sizeof(pthread_mutex_t));

  // init memory for mutex
  m = mmap(NULL, sizeof(pthread_mutex_t), mmap_prot, mmap_flags, fd, 0);

  // init mutext
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);

  pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST);
  pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);

  pthread_mutex_init(m, &attr);

  pthread_mutexattr_destroy(&attr);

  // un-map - we let the child process map it itself
  munmap(m, sizeof(pthread_mutex_t));
}

void remap_mutext() {
  m = mmap(NULL, sizeof(pthread_mutex_t), mmap_prot, mmap_flags, fd, 0);
}

void abandon_mutext() {
  pid_t fork_result = fork();
  if (fork_result == -1) {
    perror("fork");
    exit(EXIT_FAILURE);
  }

  if (fork_result == 0) {
    // child
    remap_mutext();
    pthread_mutex_lock(m);
    exit(EXIT_SUCCESS);
  } else {
    // parent
    do {
      int wait_result = waitpid(fork_result, NULL, 0);
      if (wait_result == fork_result) {
        return;
      }
      if (wait_result == -1) {
        if (errno == EINTR) {
          continue;
        } else {
          perror("wait");
          exit(EXIT_FAILURE);
        }
      }
      // ??
      fprintf(stderr, "Unexpected return from waitpid\n");
      exit(EXIT_FAILURE);
    } while (true);
  }
}
