#include <string.h>

// my_strlcpy copies at most `len - 1` bytes from `source` to
// `dest`. If we encounter a NUL in `source` the NUL will be copied
// to `dest` and the function will return. Unless `len` is `0` a
// NUL-terminator will be written to `dest`.
// This function returns `strlen(source)`.
size_t my_strlcpy(char *dest, const char *source, size_t len) {
  if (!len)
    return strlen(source);

  size_t i = 0;

  for (i = 0; i < len - 1; i++) {
    if (!(*dest++ = *source++))
      return i;
  }

  *dest = '\0';

  return strlen(source) + i;
}
