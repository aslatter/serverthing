#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "db.h"
#include "str.h"
#include "test.h"

db_t db;

typedef bool (*test_t)();
void t(test_t test, const char *title);

// test cases go here

// verify that we can write and ready data
bool basic_set() {
  set("key", "value");
  check("key", "value");
  return true;
}

// verify we clean up the db between tests
bool is_empty() { return strcmp(ez_get("key"), "value") != 0; }

// verify that binary insertion and retrieval works
bool multi_set() {
  set("key9", "value9");
  set("key8", "value8");
  set("key7", "value7");
  set("key6", "value6");
  set("key1", "value1");
  set("key2", "value2");
  set("key3", "value3");
  set("key0", "value0");

  check("key9", "value9");
  check("key8", "value8");
  check("key7", "value7");
  check("key6", "value6");
  check("key1", "value1");
  check("key2", "value2");
  check("key3", "value3");
  check("key0", "value0");

  return true;
}

// verify that over-writing keys with longer and shorter keys doesn't
// affect surrounding data.
bool over_write() {
  // data prep
  set("key1", "value1");
  set("key2", "value2");
  set("key3", "value3");

  check("key2", "value2");

  // over-write values at key2
  set("key2", "the quick brown fox jumps over the lazy dog!");
  check("key2", "the quick brown fox jumps over the lazy dog!");

  set("key2", "the quick brown fox jumps over the lazy dog!");
  check("key2", "the quick brown fox jumps over the lazy dog!");

  set("key2", "the quick brown fox jumped over the lazy dog.");
  check("key2", "the quick brown fox jumped over the lazy dog.");

  set("key2", "x");
  check("key2", "x");

  // validate other elements are still okay
  check("key1", "value1");
  check("key3", "value3");

  return true;
}

// verify keys may contain nulls
bool key_nulls() {
  // data prep
  set0("a\0", 2, "value1");
  set0("a\0a", 3, "value2");
  set0("a\0b", 3, "value3");
  set0("a\0c", 3, "value4");

  // validate we saved data at four unique keys
  check0("a\0", 2, "value1");
  check0("a\0a", 3, "value2");
  check0("a\0b", 3, "value3");
  check0("a\0c", 3, "value4");

  return true;
}

// verify key-deletion works
bool deletes() {
  set("key0", "value0");
  set("key1", "value1");
  set("key2", "value2");

  del("key1");

  check("key0", "value0");
  check("key2", "value2");

  if (strcmp(ez_get("key1"), "") != 0) {
    fprintf(stderr, "Key unexpectedly had data: %s: \"%s\"\n", "key1",
            ez_get("key1"));
    return false;
  }

  set("key1", "value1 (again)");

  check("key0", "value0");
  check("key1", "value1 (again)");
  check("key2", "value2");

  return true;
}

// verify reclaiming space in blocks works
bool test_defrag() {

  // set enough data to fill page
  char long_data[1000];
  memset(long_data, 'x', sizeof(long_data) - 1);
  long_data[0] = 'a';
  long_data[sizeof(long_data) - 2] = 'a';
  long_data[sizeof(long_data) - 1] = '\0';

  set("aa", long_data);
  set("bb", long_data);
  set("cc", long_data);
  set("dd", long_data);
  set("ee", long_data);
  set("ff", long_data);
  set("gg", long_data);

  // at this point setting more long data should fail
  if (ez_set("hh", long_data)) {
    red("Unexpectedly able to set data!");
    return false;
  }

  // over-write existing keys with shorter data
  char short_data[10];
  memset(short_data, 'y', sizeof(short_data) - 1);
  short_data[0] = 'b';
  short_data[sizeof(short_data) - 2] = 'b';
  short_data[sizeof(short_data) - 1] = '\0';

  set("aa", short_data);
  set("bb", short_data);
  set("cc", short_data);
  set("dd", short_data);
  set("ee", short_data);
  set("ff", short_data);
  set("gg", short_data);

  // now we should have more room for long_data
  set("a", long_data);
  set("b", long_data);
  set("c", long_data);
  set("d", long_data);

  // validate we have what we should
  check("aa", short_data);
  check("bb", short_data);
  check("cc", short_data);
  check("dd", short_data);
  check("ee", short_data);
  check("ff", short_data);
  check("gg", short_data);

  check("a", long_data);
  check("b", long_data);
  check("c", long_data);
  check("d", long_data);

  return true;
}

// tests of internal APIs

// verify internal version of strlcpy works as expected
bool simple_cpy() {
  char b[100];

  if (!size_expect(my_strlcpy(b, "hey", 100), 3))
    return false;
  if (!str_expect(b, "hey"))
    return false;

  if (!size_expect(my_strlcpy(b, "hey", 1), 3))
    return false;
  if (!str_expect(b, ""))
    return false;

  if (!size_expect(my_strlcpy(b, "hey", 2), 3))
    return false;
  if (!str_expect(b, "h"))
    return false;

  // set sentinal value so we can verify 0-len copy does nothing
  b[0] = 'X';
  my_strlcpy(b, "hey", 0);
  if (b[0] != 'X')
    return false;

  return true;
}

// test cases also go here

void tests() {
  t(basic_set, "get and set one key");
  t(is_empty, "begins test empty");
  t(multi_set, "sequence of sets");
  t(over_write, "over writing keys");
  t(key_nulls, "nulls in keys work");
  t(deletes, "deletes work");
  t(test_defrag, "defrag");

  t(simple_cpy, "strlcpy - simple");
}

// fin

size_t data_len = -1;
char data[1000];

bool ez_set(const char *key, const char *value) {
  return ez_set0(key, strlen(key), value);
}

char *ez_get(const char *key) { return ez_get0(key, strlen(key)); }

bool ez_delete(const char *key) {
  if (db_delete(&db, key, strlen(key)) != 0) {
    perror("delete data");
    return false;
  }
  return true;
}

bool ez_set0(const char *key, size_t key_len, const char *value) {
  if (db_set_data(&db, key, key_len, value, strlen(value)) < 0) {
    if (errno) {
      perror("set data");
    }
    return false;
  }
  return true;
}

char *ez_get0(const char *key, size_t key_len) {
  ssize_t copied = db_get_data(&db, key, key_len, data, sizeof(data) - 1);
  if (copied < 0) {
    data[0] = '\0';
    return data;
  }
  data[copied] = '\0';
  return data;
}

bool use_color = true;

void init() {
  char *path = "test.db";
  char *error;

  int fd = db_create_at_path(path, &error);
  if (fd == -1) {
    perror(error);
    unlink(path);
    exit(EXIT_FAILURE);
  }

  unlink(path);

  if (db_open(fd, &db) != 0) {
    perror("db_open");
    exit(EXIT_FAILURE);
  }

  close(fd);

  use_color = isatty(STDOUT_FILENO);
}

void cleanup() { db_close(&db); }

int test_case_cleanup() {
  memset(data, '\0', sizeof(data));
  return db_clear(&db);
}

int tests_passed = 0;
int tests_failed = 0;
int tests_total = 0;

void t(test_t test, const char *title) {
  if (test()) {
    green("PASSED - %s", title);
    tests_passed++;
  } else {
    red("FAILED - %s", title);
    tests_failed++;
  }
  tests_total++;
  test_case_cleanup();
}

void print_results() {
  printf("\n");
  if (tests_passed > 0) {
    green("%d tests passed", tests_passed);
  }
  if (tests_failed > 0) {
    red("%d tests failed", tests_failed);
  }
  printf("%d total tests\n", tests_total);
}

int main() {

  init();
  tests();
  cleanup();
  print_results();

  if (tests_failed != 0) {
    exit(EXIT_FAILURE);
  }
  return 0;
}

__attribute__((format(printf, 2, 0))) void color(int color, const char *format,
                                                 va_list ap);

// green prints a green-colored message. Will append a newline to the output.
void green(const char *format, ...) {
  va_list ap;
  va_start(ap, format);
  color(32, format, ap);
  va_end(ap);
}

// red prints a red-colored message. Will append a newline to the output.
void red(const char *format, ...) {
  va_list ap;
  va_start(ap, format);
  color(31, format, ap);
  va_end(ap);
}

// color prints a colored message to the terminal. The color is specified by
// an ANSI color-code specified in 'color'. The message is printed uncolored
// if stdout is not a terminal device. A newline will be appended to the
// output.
void color(int color, const char *format, va_list ap) {
  if (use_color) {
    printf("\x1b[%d;1m", color);
  }

  vprintf(format, ap); // NOLINT - clang-tidy thinks `ap` is not initialized?!

  if (use_color) {
    printf("\x1b[0m\n");
  } else {
    printf("\n");
  }
}

// Other test helpers

bool size_expect(size_t actual, size_t expected) {
  if (actual == expected)
    return true;

  red("Expected %zu, got %zu", expected, actual);

  return false;
}

bool str_expect(const char *actual, const char *expected) {
  if (!strcmp(actual, expected))
    return true;

  red("Expected '%s', got '%s'", expected, actual);

  return false;
}
