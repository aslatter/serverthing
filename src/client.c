#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include "db.h"
#include "io.h"
#include "str.h"

// some forward declarations for helper functions

void parse_args(char **argv);

// run_verb performs the command-line action requested
// by the user.
void run_verb(db_t *db);

// run_get performs our get operation, and prints
// the result to the terminal.
void run_get(db_t *db);

// program state

enum {
  verb_unknown,
  verb_set,
  verb_get,
  verb_delete,
  verb_fault,
  verb_dump,
} prog_verb = verb_unknown;

#define MAX_DATA 1000
char prog_data[MAX_DATA];
ssize_t prog_data_len = -1;

#define MAX_KEY 500
char prog_key[MAX_KEY + 1];
ssize_t prog_key_len = -1;

int main(int argc, char **argv) {
  (void)argc;
  parse_args(argv);

  // TODO - get path from args
  const char *path = "test.sock";

  // Set up socket & connect
  int s = socket(AF_UNIX, SOCK_STREAM, 0);
  if (s == -1) {
    perror("setup");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;

  if (strlen(path) > sizeof(addr.sun_path) - 1) {
    fprintf(stderr, "setup: path to socket is too long!\n");
    exit(EXIT_FAILURE);
  }

  my_strlcpy(addr.sun_path, path, sizeof(addr.sun_path));

  if (connect(s, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("connect");
    exit(EXIT_FAILURE);
  }

  // receive message
  int shm_fd = -1;
  if (receive_fd(s, &shm_fd) == -1) {
    perror("receive_fd");
    close(s);
    exit(EXIT_FAILURE);
  }

  if (shm_fd == -1) {
    printf("Did not receive a file-descriptor from server.\n");
    close(s);
    exit(EXIT_FAILURE);
  }

  db_t db;
  if (db_open(shm_fd, &db) == -1) {
    perror("Error opening db");
  } else {

    run_verb(&db);

    if (db_close(&db) == -1) {
      perror("Error closing db");
    }
  }

  // done!
  close(shm_fd);
  close(s);

  exit(EXIT_SUCCESS);
}

void parse_args(char **argv) {

  // don't care about prog name
  if (!*argv++) {
    // ?!
    printf("Not enough arguments\n");
    exit(EXIT_FAILURE);
  }

  char *verb_string;
  if (!(verb_string = *argv++)) {
    printf("Not enough arguments\n");
    exit(EXIT_FAILURE);
  }

  if (strcasecmp(verb_string, "get") == 0) {
    prog_verb = verb_get;
  } else if (strcasecmp(verb_string, "set") == 0) {
    prog_verb = verb_set;
  } else if (strcasecmp(verb_string, "delete") == 0) {
    prog_verb = verb_delete;
  } else if (strcasecmp(verb_string, "fault") == 0) {
    prog_verb = verb_fault;
  } else if (strcasecmp(verb_string, "dump") == 0) {
    prog_verb = verb_dump;
  } else {
    printf("Unknown command \"%s\"\n", verb_string);
    exit(EXIT_FAILURE);
  }

  if (prog_verb == verb_fault || prog_verb == verb_dump) {
    // 'fault' and 'dump' don't take args so we're done
    return;
  }

  // key is next for any remaining verb
  char *key_str;
  if (!(key_str = *argv++)) {
    printf("Not enough arguments\n");
    exit(EXIT_FAILURE);
  }

  const size_t key_len = strlen(key_str);

  bool key_to_big = false;
  const char *key_size_err = "Key too large";
  if (key_len > MAX_KEY) {
    key_to_big = true;
  } else {
    // TODO - why am I copying here?
    strcpy(prog_key, key_str);
    prog_key_len = key_len;
  }

  // GET & DELETE
  if (prog_verb == verb_get || prog_verb == verb_delete) {
    if (key_to_big) {
      printf("%s\n", key_size_err);
      exit(EXIT_FAILURE);
    }
    return;
  }

  // SET
  if (prog_verb == verb_set) {
    char *data;
    if (!(data = *argv++)) {
      printf("Not enough arguments\n");
      exit(EXIT_FAILURE);
    }

    if (key_to_big) {
      printf("%s\n", key_size_err);
      exit(EXIT_FAILURE);
    }

    const size_t len = strlen(data);
    if (len + 1 > MAX_DATA) {
      printf("Data to store is too long\n");
      exit(EXIT_FAILURE);
    }

    // TODO - again, why copy?
    my_strlcpy(prog_data, data, sizeof(prog_data));
    prog_data_len = len;
    return;
  }

  // ?!
  printf("Unknown command\n");
  exit(EXIT_FAILURE);
}

void run_verb(db_t *db) {
  // we assume we're in a sane state
  switch (prog_verb) {
  case verb_get:
    run_get(db);
    return;
  case verb_set:
    if (db_set_data(db, prog_key, prog_key_len, prog_data, prog_data_len) ==
        -1) {
      perror("Error setting data to db");
    }
    return;
  case verb_delete:
    if (db_delete(db, prog_key, prog_key_len) != 0) {
      perror("Error deleting data");
    }
    return;
  case verb_fault:
    if (db_cause_fault(db) == -1) {
      perror("db_cause_fault");
    }
    return;
  case verb_dump:
    db_dump(db);
    return;
  default:
    fprintf(stdout, "Unknown verb: %d\n", prog_verb);
    exit(EXIT_FAILURE);
  }
}

void run_get(db_t *db) {
  if (prog_data_len = db_get_data(db, prog_key, prog_key_len, prog_data,
                                  sizeof(prog_data) - 1),
      prog_data_len < 0) {
    printf("Error getting data from db\n");
    perror("get_data");
    return;
  }
  if (prog_data_len < (ssize_t)sizeof(prog_data)) {
    prog_data[prog_data_len + 1] = '\0';
    printf("Received: %s\n", prog_data);
  }
}
