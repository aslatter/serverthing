
#pragma once

#include <pthread.h>
#include <sys/types.h>

#define DB_MAX_KEY 1024
#define DB_MAX_VALUE (5 * 1024)

// The actual data in shared memory
typedef struct db_shared_t db_shared_t;

// Client-handle to mapped data
typedef struct db_t {
  // enough information to call `munmap`
  size_t map_len;

  // actual mapped data
  db_shared_t *shared;
} db_t;

// db_create returns an fd to memory which holds
// an initialized `db_shared_t`.
extern int db_create_shared(char **err_msg);

// db_create_at_path returns an fd open to `path` which
// holds a newly initialized `db_shared_t`.
extern int db_create_at_path(char *path, char **err_msg);

// db_open connects to the database backed by
// the file 'fd'. We map the file and set up
// the fields of *db for access to the database.
// You may close `fd` after calling `db_open`.
extern int db_open(int fd, db_t *db);

// db_close closes the database opened by `db_open`.
extern int db_close(db_t *db);

// db_set_data sets `data` into the database. The database
// can currently only hold a single string. This function
// replaces the current string.
extern ssize_t db_set_data(db_t *db, const char *key, size_t key_len,
                           const char *data, size_t data_len);

// db_get_data retrieves the data set by `db_set_data`.
extern ssize_t db_get_data(db_t *db, const char *key, size_t key_len,
                           char *data, size_t data_len);

// db_delete removes a key from the database
extern int db_delete(db_t *db, const char *key, size_t key_len);

// db_cause_fault should not be used, really.
extern int db_cause_fault(db_t *db);

// drop all data in the database. Used for testing.
extern int db_clear(db_t *db);

// db_dump prints the contents of the db to stdout.
extern void db_dump(db_t *db);
