#pragma once

#include <stdbool.h>
#include <string.h>

/*
Functionality in `test.c` is not intended to be used externally. This header
file only exists to make forward-declarations cleaner.

Some forward declarations are left in test.c either for clarity or because
they were only needed 'bellow the fold' (i.e. after our test-cases).
 */

__attribute__((format(printf, 1, 2))) void green(const char *format, ...);

__attribute__((format(printf, 1, 2))) void red(const char *format, ...);

bool ez_set(const char *key, const char *value);
char *ez_get(const char *key);
bool ez_delete(const char *key);

bool ez_set0(const char *key, size_t key_len, const char *value);
char *ez_get0(const char *key, size_t key_len);

#define set(key, value)                                                        \
  do {                                                                         \
    if (!ez_set((key), (value)))                                               \
      return false;                                                            \
  } while (false)

#define check(key, value)                                                      \
  do {                                                                         \
    if (strcmp(ez_get(key), (value)) != 0) {                                   \
      red("Expected \"%s\", got \"%s\"", (value), ez_get((key)));              \
      return false;                                                            \
    }                                                                          \
  } while (false)

#define del(key)                                                               \
  do {                                                                         \
    if (!ez_delete((key))) {                                                   \
      return false;                                                            \
    }                                                                          \
  } while (false)

#define set0(key, key_len, value)                                              \
  do {                                                                         \
    if (!ez_set0((key), (key_len), (value)))                                   \
      return false;                                                            \
  } while (false)

#define check0(key, key_len, value)                                            \
  do {                                                                         \
    if (strcmp(ez_get0((key), (key_len)), (value)) != 0)                       \
      return false;                                                            \
  } while (false)

// test helpers

bool size_expect(size_t actual, size_t expected);
bool str_expect(const char *actual, const char *expected);
