#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>

#include "io.h"

// send_fd sends a single byte along socket 's' as well
// as sending `fd` along as ancillary data.
ssize_t send_fd(int s, int fd) {

  // notes:
  //   sendmsg - function which can also send ancillary data
  //   recvmsg - function which can also receive ancillary data
  //   SOL_SOCKET - specifier for sending an fd group
  //   cmsghdr.cmsg_level - set to SOL_SOCKET
  //   cmsghdr.cmsg_type - set to SCM_RIGHTS
  // for SCM_RIGHTS the 'data' portion contains an integer array
  //  of file-descriptors

  // man pages of interest:
  //  sendmsg(3)
  //  cmsg(3)
  //  unix(7)

  struct msghdr message = {0};

  // Set up message. We don't actually care about this,
  // but we need to send something.
  struct iovec vec = {0};
  vec.iov_base = "\0";
  vec.iov_len = 1;

  message.msg_iov = &vec;
  message.msg_iovlen = 1;

  // set up ancillary data

  /* Ancillary data buffer, wrapped in a union
     in order to ensure it is suitably aligned */
  union {
    char buf[CMSG_SPACE(sizeof(fd))];
    struct cmsghdr align;
  } u;

  message.msg_control = u.buf;
  message.msg_controllen = sizeof(u.buf);

  struct cmsghdr *cmsg = CMSG_FIRSTHDR(&message);
  cmsg->cmsg_level = SOL_SOCKET;
  cmsg->cmsg_type = SCM_RIGHTS;
  cmsg->cmsg_len = CMSG_LEN(sizeof(fd));

  // add data to payload
  memcpy(CMSG_DATA(cmsg), &fd, sizeof(fd));

  // done!
  int flags = 0;
  return sendmsg(s, &message, flags);
}

// receive_fd receives a file-descriptor `*fd`
// in the ancillary data of file-socket `s`.
// Returns `1` if we were able to successfully receive
// data on `s`, or `-1` on failure. If the received data
// did not contain a file-descriptor in its ancillary data
// *fd will be set to `-1`).
ssize_t receive_fd(int s, int *fd) {
  if (!fd) {
    errno = EINVAL;
    return -1;
  }
  *fd = -1;

  // We must receive some non oob data,
  // so we leave one byte for that.
  char io_buf[1];
  struct iovec iov = {0};
  iov.iov_base = io_buf;
  iov.iov_len = sizeof(io_buf);

  // should be long enough?!
  char control_buf[1000];

  // actual msgheader struct
  struct msghdr message = {0};

  message.msg_iov = &iov;
  message.msg_iovlen = 1;

  message.msg_control = control_buf;
  message.msg_controllen = sizeof(control_buf);

  int flags = 0;
  if (recvmsg(s, &message, flags) == -1) {
    // leave errno for caller
    return -1;
  }

  // loop through ancillary data until we get
  // what we're after
  struct cmsghdr *cmsg;
  for (cmsg = CMSG_FIRSTHDR(&message); cmsg != NULL;
       cmsg = CMSG_NXTHDR(&message, cmsg)) {

    if (cmsg->cmsg_level != SOL_SOCKET) {
      continue;
    }
    if (cmsg->cmsg_type != SCM_RIGHTS) {
      continue;
    }
    // looks good so far
    memcpy(fd, CMSG_DATA(cmsg), sizeof(*fd));
    break;
  }

  // We return '1' if we received *something* even
  // if we didn't get file-descriptor out of the
  // ancillary data - the caller needs to check the
  // output fd for '-1'.
  return 1;
}
