
#pragma once

#include <sys/types.h>

extern ssize_t send_fd(int sock, int fd);
extern ssize_t receive_fd(int sock, int *fd);
