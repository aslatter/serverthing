#define _GNU_SOURCE
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include "db.h"
#include "io.h"
#include "str.h"

void init_shm();
void init_un(const char *);
void handle_clients();
void handle_signal(int);
void init_signals();

// Descriptor for shared memory
int shm_fd = -1;

// Open file-socket
int fileSocket = -1;

int main() {

  // TODO - get path from args
  const char *path = "test.sock";

  init_shm();
  init_un(path);
  init_signals();

  printf("Done with init ...\n");

  handle_clients();

  printf("Done handling clients ...\n");

  unlink(path); // nothing to do if this fails

  exit(EXIT_SUCCESS);
}

void init_shm() {
  char *error_message;
  shm_fd = db_create_shared(&error_message);
  if (shm_fd == -1) {
    perror(error_message);
    exit(EXIT_FAILURE);
  }
}

void init_un(const char *path) {

  // Attempt to clean up previous run
  struct stat pstat;
  if (stat(path, &pstat) == -1) {
    if (errno != ENOENT) {
      perror("init_un: stat");
      exit(EXIT_FAILURE);
    }
  } else if (S_ISSOCK(pstat.st_mode)) {
    // this is a bit racey - I'm not sure what the right thing is, though :-/
    unlink(path);
  }

  fileSocket = socket(AF_UNIX, SOCK_STREAM, 0);
  if (fileSocket == -1) {
    perror("init: socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;

  if (strlen(path) > sizeof(addr.sun_path) - 1) {
    fprintf(stderr, "init: path to socket is too long!\n");
    exit(EXIT_FAILURE);
  }

  my_strlcpy(addr.sun_path, path, sizeof(addr.sun_path));

  if (bind(fileSocket, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("init: bind");
    exit(EXIT_FAILURE);
  }

  const int backlog = 5;
  if (listen(fileSocket, backlog) == -1) {
    perror("init: listen");
    exit(EXIT_FAILURE);
  }
}

bool received_stop = false;

void handle_clients() {
  int client;
  while (client = accept(fileSocket, NULL, NULL), !received_stop) {

    // Some failures are okay
    if (client == -1) {
      printf("Failed for some reason ...\n");
      if (errno == ECONNABORTED || errno == EINTR) {
        // we shouldn't get here if we received interrupt, but we may as well
        // check
        if (received_stop) {
          break;
        } else {
          continue;
        }
      } else {
        perror("handle_client: accept");
        exit(EXIT_FAILURE);
      }
    }

    // Handle connected client
    printf("Client connected ...\n");

    if (send_fd(client, shm_fd) == -1) {
      // hmmm ... not sure what to do here? We close the connection after this
      // anyway.
      perror("send_fd");
    }

    close(client);
    printf("Done with client ...\n");
  }

  if (client != -1) {
    close(client);
  }
  close(fileSocket);
}

void init_signals() {
  struct sigaction saction;
  memset(&saction, 0, sizeof(struct sigaction));
  saction.sa_handler = handle_signal;

  // Handle TERM and INT identically
  if (sigaction(SIGINT, &saction, NULL) == -1) {
    perror("init: sigaction");
    exit(EXIT_FAILURE);
  }

  if (sigaction(SIGTERM, &saction, NULL) == -1) {
    perror("init: sigaction");
    exit(EXIT_FAILURE);
  }

  // Ignore SIGPIPE (let send return EPIPE)
  saction.sa_handler = SIG_IGN;
  if (sigaction(SIGPIPE, &saction, NULL) == -1) {
    perror("init: sigaction");
    exit(EXIT_FAILURE);
  }
}

void handle_signal(int signal) {
  if (signal == SIGINT || signal == SIGTERM) {
    received_stop = true;
  }
}
