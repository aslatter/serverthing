// for sys/mman.h
#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "db.h"

// internal data structures

#define DB_PAGE_COUNT 50
#define DB_PAGE_SIZE (7 * 1024)

// This is what is actually mapped to shared memory
typedef struct db_shared_t {
  pthread_mutex_t m;
  uint32_t page_count;
  char data[DB_PAGE_SIZE * DB_PAGE_COUNT];
} db_shared_t;

enum db_result {
  db_result_okay,
  db_result_error,
  db_result_key_too_big,
  db_result_val_too_big,
  db_result_page_full,
};

// == Any Page ==

typedef struct db_any_t {
  uint16_t page_type;
  uint16_t flags;
  uint32_t parent;
} db_any_t;

// == Internal Pages ==

typedef struct db_internal_item_t {
  uint32_t key_offset;
  uint32_t key_len;
  uint32_t child_id;
} db_internal_item_t;

typedef struct db_internal_t {
  uint16_t page_type;
  uint16_t flags;
  uint32_t parent;
  uint32_t item_count;
  uint32_t free_data_offset;
  db_internal_item_t items[];
} db_internal_t;

// == Leaf Pages ==

typedef struct db_leaf_item_t {
  uint32_t key_offset;
  uint32_t key_len;
  uint32_t data_offset;
  uint32_t data_len;
} db_leaf_item_t;

typedef struct db_leaf_t {
  uint16_t page_type;
  uint16_t flags;
  uint32_t parent;
  uint32_t item_count;
  uint32_t free_data_offset;
  db_leaf_item_t items[];
} db_leaf_t;

// == Page Type ==

typedef union db_page_t {

  db_any_t any;
  db_leaf_t leaf;
  db_internal_t internal;

  char data[DB_PAGE_SIZE];
} db_page_t;

// == Page Primitives ==

#define PAGE_TYPE_LEAF 1
#define PAGE_TYPE_INTERNAL 2

static const uint16_t PAGE_FLAG_IS_ROOT = 1 << 0;
static const uint16_t PAGE_FLAG_IS_FRAGMENTED = 1 << 1;

// forward declarations for private functions

// == db operations ==

static void db_initialize(db_shared_t *s);

static int init_server(db_shared_t *s);

// == page operations ==

static int db_page_delete(db_page_t *p, const char *key, size_t key_len);

// == leaf-page operations ==

static void db_leaf_init(db_leaf_t *p);

static void db_leaf_init_data(db_leaf_t *p);

static enum db_result db_leaf_insert(db_leaf_t *p, const char *key,
                                     size_t key_len, const char *value,
                                     size_t value_len);

static int db_leaf_delete(db_leaf_t *p, const char *key, size_t key_len);

static ssize_t db_leaf_get(const db_leaf_t *p, const char *key, size_t key_len,
                           char *data, size_t data_len);

static uint32_t db_leaf_find_key(const db_leaf_t *p, const char *key,
                                 size_t key_len, bool *exists);

static bool db_leaf_compact(db_leaf_t *p, uint32_t goal);

static uint32_t db_leaf_alloc(db_leaf_t *p, size_t len);

static uint32_t db_leaf_free_space(const db_leaf_t *p);

// == internal-page operations ==

// == other ==

static int key_compare(const char *lhs, size_t lhs_len, const char *rhs,
                       size_t rhs_len);

// implementations of public and private functions

int init_server(db_shared_t *s) {

  // initialize mutex
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);

  // allow access from multiple processes
  if (pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED) != 0) {
    pthread_mutexattr_destroy(&attr);
    return -1;
  }

  // allow cleaning up after dead processes
  // TODO - this doesn't seem to be working :-(
  if (pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST) != 0) {
    pthread_mutexattr_destroy(&attr);
    return -1;
  }

  pthread_mutex_init(&s->m, &attr);
  pthread_mutexattr_destroy(&attr);

  // initialize database
  db_initialize(s);

  return 0;
}

static char error_storage[1000];

static bool db_create_internal(int fd, char **err_msg);

// db_create_shared returns an initialized file-descriptor for a fresh
// shared-memory segment containing a fully set-up database.
// Will return `-1` on error, with `errno` still set to an
// error-code related to the problem. On failure will also return
// a message in `err_message` related to the operation which was
// in progress when we failed, and can be passed to `perror` or
// the like.
int db_create_shared(char **err_msg) {
  // set up shared-memory segment
  int shm_fd = memfd_create("test server memory", 0);
  if (shm_fd == -1) {
    if (err_msg)
      *err_msg = "db_create_shared: error allocating shared memory";
    return -1;
  }

  // initialize db at fd
  if (!db_create_internal(shm_fd, err_msg)) {
    close(shm_fd);
    return -1;
  }

  return shm_fd;
}

int db_create_at_path(char *path, char **err_msg) {
  int fd = open(path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    // leave errno set for caller
    *err_msg = "db_create_at_path: error opening file";
    return -1;
  }

  if (!db_create_internal(fd, err_msg)) {
    close(fd);
    return -1;
  }

  return fd;
}

bool db_create_internal(int fd, char **err_msg) {

  if (ftruncate(fd, sizeof(db_shared_t)) == -1) {
    if (err_msg)
      *err_msg = "db_create: ftruncate failed";
    return false;
  }

  db_shared_t *s = mmap(NULL, sizeof(db_shared_t), PROT_READ | PROT_WRITE,
                        MAP_SHARED, fd, 0);
  if (s == MAP_FAILED) {
    if (err_msg)
      *err_msg = "db_create: mmap failed";
    close(fd);
    return false;
  }

  // initialize fields in `s`
  if (init_server(s) != 0) {
    // the return value may or may not be `error_storage`. If its not
    // I don't need to worry about cleaning it up, though.
    *err_msg = strerror_r(errno, error_storage, sizeof(error_storage));
    munmap(s, sizeof(db_shared_t));
    return false;
  }

  if (munmap(s, sizeof(db_shared_t)) == -1) {
    if (err_msg)
      *err_msg = "db_create: munmap";
    return false;
  }
  return true;
}

// // Client-handle to mapped data
// typedef struct db_t {
//   // enough information to call `munmap`
//   size_t map_len;
//
//   // actual mapped data
//   db_shared_t *shared;
// } db_t;
//
// // db_shared_create returns a fd to memory which holds
// // an initialized `db_shared_t`.
// extern int db_shared_create(char **err_msg);
//
// extern int db_open(int fd, db_t *db);
// extern int db_close(db_t *db);
//
// extern int db_set_data(db_t *db, char *data, size_t data_len);
// extern int db_get_data(db_t *db, char *data, size_t data_len);

int db_open(int fd, db_t *db) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }

  const int prot = PROT_READ | PROT_WRITE;
  const int flags = MAP_SHARED;
  const int offset = 0;
  const int map_len = sizeof(db_shared_t);
  db_shared_t *mapped = mmap(NULL, map_len, prot, flags, fd, offset);
  if (mapped == MAP_FAILED) {
    // leave errno for caller
    return -1;
  }

  db_t empty = {0};
  *db = empty;

  db->map_len = map_len;
  db->shared = mapped;

  return 0;
}

int db_close(db_t *db) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }
  if (!db->shared) {
    errno = EINVAL;
    return -1;
  }

  if (munmap(db->shared, db->map_len) == -1) {
    // leave errno for caller
    return -1;
  }

  db_t empty = {0};
  *db = empty;

  return 0;
}

// db_lock acquires the global db lock.
// This function does no sanity checking on `db`.
int db_lock(db_t *db) {

  db_shared_t *s = db->shared;
  int lock_result = pthread_mutex_lock(&s->m);

  if (lock_result == 0) {
    return 0;
  }

  if (lock_result == EOWNERDEAD) {
    // last owner left the lock hanging.
    // nuke the db because we don't know what state it's in.
    db_initialize(s);
    if (pthread_mutex_consistent(&s->m) != 0) {
      // ?!
      pthread_mutex_unlock(&s->m);
      return -1;
    } else {
      // recoverd okay
      return 0;
    }
  }

  // leave errno set for caller
  return lock_result;
}

// db_unlock releases the lock acquired by db_lock.
int db_unlock(db_t *db) { return pthread_mutex_unlock(&db->shared->m); }

// db_page_set inserts data into a database page
static enum db_result db_page_set(db_page_t *p, const char *key, size_t key_len,
                                  const char *data, size_t data_len) {

  switch (p->any.page_type) {

  case PAGE_TYPE_LEAF:
    return db_leaf_insert(&p->leaf, key, key_len, data, data_len);

  case PAGE_TYPE_INTERNAL:
    fputs("db_set unimplemented for internal pages!\n", stderr);
    return db_result_error;
  }

  fprintf(stderr, "db_set: Unexpected page-type '%d'\n", p->any.page_type);
  return db_result_error;
}

ssize_t db_set_data(db_t *db, const char *key, size_t key_len, const char *data,
                    size_t data_len) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }
  if (!db->shared) {
    errno = EINVAL;
    return -1;
  }
  if (!data) {
    return -1;
  }
  if (data_len > DB_MAX_VALUE) {
    errno = EINVAL;
    return -1;
  }
  if (!key) {
    errno = EINVAL;
    return -1;
  }
  if (key_len > DB_MAX_KEY) {
    errno = EINVAL;
    return -1;
  }

  if (db_lock(db) != 0) {
    // leave errno set for caller
    return -1;
  }

  // grab the root page (offset 0) and perform the insert
  enum db_result result =
      db_page_set((db_page_t *)db->shared->data, key, key_len, data, data_len);

  if (db_unlock(db) != 0) {
    // leave errno set for caller
    return -1;
  }
  return result == db_result_okay ? 0 : -1;
}

static void page_type_err(const char *fn_name, int page_type) {
  fprintf(stderr, "%s: Unexpected page-type %d\n", fn_name, page_type);
}

int db_delete(db_t *db, const char *key, size_t key_len) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }

  if (!db->shared) {
    errno = EINVAL;
    return -1;
  }

  if (!key) {
    errno = EINVAL;
    return -1;
  }

  if (db_lock(db) != 0) {
    return -1;
  }

  int retval = db_page_delete((db_page_t *)db->shared->data, key, key_len);

  if (db_unlock(db) != 0) {
    return -1;
  }

  return retval;
}

static int db_page_delete(db_page_t *p, const char *key, size_t key_len) {
  switch (p->any.page_type) {

  case PAGE_TYPE_LEAF:
    return db_leaf_delete(&p->leaf, key, key_len);

  case PAGE_TYPE_INTERNAL:
    fputs("db_page_delete internal: unimplemented\n", stderr);
    return -1;
  }

  page_type_err("db_page_delete", p->any.page_type);
  return -1;
}

static int db_leaf_delete(db_leaf_t *p, const char *key, size_t key_len) {

  bool exists = false;
  uint32_t item = db_leaf_find_key(p, key, key_len, &exists);

  if (exists) {
    memmove(p->items + item, p->items + item + 1,
            (p->item_count - item - 1) * sizeof(p->items[0]));
    p->item_count--;
    p->flags |= PAGE_FLAG_IS_FRAGMENTED;
  }

  return 0;
}

static ssize_t db_page_get(db_page_t *p, const char *key, size_t key_len,
                           char *data, ssize_t data_len) {
  switch (p->any.page_type) {

  case PAGE_TYPE_LEAF:
    return db_leaf_get(&p->leaf, key, key_len, data, data_len);

  case PAGE_TYPE_INTERNAL:
    fputs("db_get internal: unimplemented\n", stderr);
    return -1;
  }

  page_type_err("db_page_get", p->any.page_type);
  return -1;
}

ssize_t db_get_data(db_t *db, const char *key, size_t key_len, char *data,
                    size_t data_len) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }
  if (!db->shared) {
    errno = EINVAL;
    return -1;
  }
  if (key_len > DB_MAX_KEY) {
    errno = EINVAL;
    return -1;
  }
  if (!key) {
    errno = EINVAL;
    return -1;
  }
  if (!data) {
    errno = EINVAL;
    return -1;
  }

  if (db_lock(db) != 0) {
    // leave errno set for caller
    return -1;
  }

  const size_t copy_len = db_page_get((db_page_t *)(db->shared->data), key,
                                      key_len, data, data_len);

  if (db_unlock(db) != 0) {
    // leaver errno set for caller
    return -1;
  }

  return copy_len;
}

// db_initialize re-initializes the database. This entry-point
// assumes correct inputs, and that locks have been acquired.
static void db_initialize(db_shared_t *s) {
  db_leaf_t *p = (db_leaf_t *)s->data;
  db_leaf_init(p);
  p->flags |= PAGE_FLAG_IS_ROOT;

  s->page_count = 1;
}

// db_clear drops all data in `db`
int db_clear(db_t *db) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }
  if (!db->shared) {
    errno = EINVAL;
    return -1;
  }

  if (db_lock(db) != 0) {
    return -1;
  }

  db_initialize(db->shared);

  if (db_unlock(db) != 0) {
    return -1;
  }

  return 0;
}

// db_cause_fault acquires the global db lock and does not release
// it. This is only useful for testing how we recovery from a pid
// going away in the middle of the critical section.
int db_cause_fault(db_t *db) {
  if (!db) {
    errno = EINVAL;
    return -1;
  }

  if (db_lock(db) != 0) {
    return -1;
  }

  exit(EXIT_SUCCESS);
}

// db_leaf_init preps a page for use and correctly zeros its storage.
void db_leaf_init(db_leaf_t *p) {
  p->page_type = PAGE_TYPE_LEAF;
  p->flags = 0;
  p->parent = 0;
  p->item_count = 0;
  db_leaf_init_data(p);
}

void db_leaf_init_data(db_leaf_t *p) { p->free_data_offset = DB_PAGE_SIZE - 1; }

// db_leaf_free_space gets the amount of free-space in bytes for a page.
uint32_t db_leaf_free_space(const db_leaf_t *p) {
  uint32_t top = sizeof(db_leaf_t) + (p->item_count * sizeof(db_leaf_item_t));
  uint32_t bottom = p->free_data_offset;
  return bottom - top;
}

// db_leaf_insert sets a k/v pair into a page. If the key exists it will
// be over-written.
enum db_result db_leaf_insert(db_leaf_t *p, const char *key, size_t key_len,
                              const char *value, size_t value_len) {
  if (key_len > DB_MAX_KEY) {
    return db_result_key_too_big;
  }
  if (value_len > DB_MAX_VALUE) {
    return db_result_val_too_big;
  }

  size_t required_space = sizeof(db_leaf_item_t) + key_len + value_len;
  uint32_t free_space = db_leaf_free_space(p);
  if (free_space < required_space) {
    if (!(p->flags & PAGE_FLAG_IS_FRAGMENTED) ||
        !db_leaf_compact(p, required_space)) {
      return db_result_page_full;
    }
  }

  bool exists = false;
  uint32_t item_location = db_leaf_find_key(p, key, key_len, &exists);

  if (!exists && item_location < p->item_count) {
    memmove(p->items + item_location + 1, p->items + item_location,
            sizeof(db_leaf_item_t) * (p->item_count - item_location));
  }

  db_leaf_item_t *item = p->items + item_location;

  char *page_data = (char *)p;

  if (!exists) {
    p->item_count++;

    // write a new key
    uint32_t key_offset = db_leaf_alloc(p, key_len);
    memcpy(page_data + key_offset, key, key_len);
    item->key_offset = key_offset;
    item->key_len = key_len;
  }

  // write new data
  if (exists && item->data_len >= value_len) {
    memcpy(page_data + item->data_offset, value, value_len);
    item->data_len = value_len;
  } else {
    uint32_t data_offset = db_leaf_alloc(p, value_len);
    memcpy(page_data + data_offset, value, value_len);
    item->data_offset = data_offset;
    item->data_len = value_len;
  }

  if (exists) {
    p->flags |= PAGE_FLAG_IS_FRAGMENTED;
  }

  return db_result_okay;
}

// db_leaf_compact attempts to free up space in the page by smooshing
// around data in to free space in the page. `goal` is the desired amount
// of space we'd like to have when we're done. We will skip compaction if
// we cannot achieve the goal.
bool db_leaf_compact(db_leaf_t *p, uint32_t goal) {
  // calculate potential savings
  uint32_t current = sizeof(db_leaf_t);
  current += sizeof(db_leaf_item_t) * p->item_count;
  for (uint i = 0; i < p->item_count; i++) {
    db_leaf_item_t *item = &p->items[i];
    current += item->key_len + item->data_len;
  }

  uint32_t free_space = sizeof(db_page_t) - current;
  if (free_space < goal) {
    return false;
  }

  // we make a new page, copy bits to it, then copy them back
  db_leaf_t *tmp = malloc(DB_PAGE_SIZE);

  memcpy(tmp, p, sizeof(*p) + (p->item_count * sizeof(p->items[0])));
  db_leaf_init_data(tmp);
  tmp->flags &= ~PAGE_FLAG_IS_FRAGMENTED;

  for (uint i = 0; i < p->item_count; i++) {
    db_leaf_item_t *item = &p->items[i];
    db_leaf_item_t *t_item = &tmp->items[i];

    uint32_t key_offset = db_leaf_alloc(tmp, t_item->key_len);
    uint32_t data_offset = db_leaf_alloc(tmp, t_item->data_len);

    t_item->key_offset = key_offset;
    t_item->data_offset = data_offset;

    memcpy((char *)tmp + key_offset, (char *)p + item->key_offset,
           t_item->key_len);
    memcpy((char *)tmp + data_offset, (char *)p + item->data_offset,
           t_item->data_len);
  }

  // copy back
  memcpy(p, tmp, sizeof(db_page_t));
  free(tmp);

  return true;
}

uint32_t db_leaf_alloc(db_leaf_t *p, size_t len) {
  uint32_t offset = p->free_data_offset - (len - 1);
  p->free_data_offset -= len;
  return offset;
}

// db_leaf_get retrieves the data set at a given key, and returns
// the length of the found data (up to a max of `data_len`), or `-1`
// if the key was not found in the page.
ssize_t db_leaf_get(const db_leaf_t *p, const char *key, size_t key_len,
                    char *data, size_t data_len) {
  bool exists = false;
  uint32_t position = db_leaf_find_key(p, key, key_len, &exists);
  if (!exists) {
    return -1;
  }

  const db_leaf_item_t *item = p->items + position;

  uint32_t copy_size = item->data_len;
  copy_size = copy_size > data_len ? data_len : copy_size;

  const char *page_data = (char *)p;
  memcpy(data, page_data + item->data_offset, copy_size);

  return copy_size;
}

static void bytesOut(const char *bytes, size_t len) {
  fwrite(bytes, 1, len, stdout);
}

// db_dump_leaf_item prints to stdout the key and value for
// an item.
static void db_dump_leaf_item(const db_leaf_t *p, uint32_t item_num) {

  const db_leaf_item_t *item = (p->items) + item_num;
  const char *data = (const char *)p;

  bytesOut(data + item->key_offset, item->key_len);
  fputs(": ", stdout);
  bytesOut(data + item->data_offset, item->data_len);
  fputs("\n", stdout);
}

// db_dump_leaf prints to stdout a debugging dump of the contents of
// a page.
static void db_dump_leaf(const db_leaf_t *p) {
  printf("\nDumping db page ...\n");

  for (uint32_t i = 0; i < p->item_count; i++) {
    db_dump_leaf_item(p, i);
  }

  printf("\nDone!\n");
}

static void db_dump_page(const db_page_t *p) {
  switch (p->any.page_type) {

  case PAGE_TYPE_LEAF:
    db_dump_leaf(&p->leaf);
    break;

  case PAGE_TYPE_INTERNAL:
    fputs("db_dump_page: page-type internal: unimplemented\n", stderr);
    break;
  }
}

// db_dump prints the contents of the database to stdout.
void db_dump(db_t *db) {
  if (!db) {
    return;
  }
  if (!db->shared) {
    return;
  }

  if (db_lock(db) != 0) {
    return;
  }

  const db_shared_t *s = db->shared;
  const db_page_t *p = (db_page_t *)s->data;
  db_dump_page(p);

  db_unlock(db);
}

// db_leaf_find_key returns the position at which the given key should be
// inserted. The 'exists' output parameter returns true if an exact match
// already exists at the returned position (making this function useful
// for reads as well as writes).
// The returned insertion point is one past the last element found whose
// key is less-than the passed-in key. This position may point after the
// last element in the page.
uint32_t db_leaf_find_key(const db_leaf_t *p, const char *key, size_t key_len,
                          bool *exists) {
  *exists = false;
  const char *page_data = (char *)p;

  if (p->item_count == 0) {
    return 0;
  }

  uint32_t min = 0;
  uint32_t max = p->item_count;
  uint32_t mid = (min + max) / 2;

  while (true) {
    const db_leaf_item_t *ref = (p->items) + mid;
    const char *ref_key = page_data + (ref->key_offset);
    const int cmp = key_compare(key, key_len, ref_key, ref->key_len);

    if (cmp == 0) {
      *exists = true;
      return mid;
    }
    if (cmp > 0) {
      if (mid == max - 1) {
        return max; // we return one past the less-than-element
      }
      min = mid;
      mid = (min + max) / 2;
    } else {
      // cmp < 0
      if (mid == min) {
        return mid; // we want to push-forward the greater-than element we found
      }
      max = mid;
      mid = (min + max) / 2;
    }
  }
  return mid;
}

// key_compare is pretty much like memcmp, except allowing the inputs
// to differ in length. Shorter inputs sort before longer inputs (all
// other bytes being equal).
int key_compare(const char *lhs, size_t lhs_len, const char *rhs,
                size_t rhs_len) {

  int tie_breaker = 0;
  size_t cmp_max = lhs_len;

  if (lhs_len > rhs_len) {
    tie_breaker = 1;
    cmp_max = rhs_len;
  } else if (lhs_len < rhs_len) {
    tie_breaker = -1;
  }

  int cmp = memcmp(lhs, rhs, cmp_max);
  if (cmp == 0) {
    return tie_breaker;
  }
  return cmp;
}
