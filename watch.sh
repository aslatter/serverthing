#!/usr/bin/env bash
set -euo pipefail

make

inotify-hookable -w src -f Makefile -c make

